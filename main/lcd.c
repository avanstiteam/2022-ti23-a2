#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
// #include "driver/gpio.h"
// #include "driver/i2c.h"
// #include "smbus.h"
#include "i2c-lcd1602.h"

#include "lcd.h"

#define LCD_NUM_ROWS			 4
#define LCD_NUM_COLUMNS			 32
#define LCD_NUM_VISIBLE_COLUMNS	 20

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       100000
#define I2C_MASTER_SDA_IO        18
#define I2C_MASTER_SCL_IO        23

void lcdInit() {
    // Set up I2C
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.clk_flags = 0;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;

    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, (&conf)->mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
    i2c_port_t i2c_num = I2C_MASTER_NUM;
    uint8_t address = 0x27;

    // Set up the SMBus
    smbus_info_t * smbus_info = smbus_malloc();
    ESP_ERROR_CHECK(smbus_init(smbus_info, i2c_num, address));
    ESP_ERROR_CHECK(smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS));

    // Set up the LCD1602 device with backlight off
    lcd_info = i2c_lcd1602_malloc();
    ESP_ERROR_CHECK(i2c_lcd1602_init(lcd_info, smbus_info, true,
                                     LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS));

    ESP_ERROR_CHECK(i2c_lcd1602_reset(lcd_info));
}


void lcdBacklightOn() {
    i2c_lcd1602_set_backlight(lcd_info, true);
}

void lcdBacklightOff() {
    i2c_lcd1602_set_backlight(lcd_info, false);
}

void lcdWriteScreen(DISPLAY_LINES lines) {
    lcdClear();
    lcdWriteToLine(lines.line1, 0);
    lcdWriteToLine(lines.line2, 1);
    lcdWriteToLine(lines.line3, 2);
    lcdWriteToLine(lines.line4, 3);
}

void lcdWriteToLine(char *text, uint8_t lineNumber) {
    uint8_t col = 0;
	for(;*text; text++){
        i2c_lcd1602_move_cursor(lcd_info, col, lineNumber);
        i2c_lcd1602_write_char(lcd_info, *text);        
        col++;
	}
}

void lcdClear() {
    i2c_lcd1602_clear(lcd_info);
}
