#pragma once

/**
 * @brief init the audio pipeline, only call this function once.
 **/
void audioPipelineInit(void);

/**
 * @brief link and start the audio pipeline.
 * @param[in] modus  2 if using soundAnalysis. 1 if using http, 0 if using sd.
 * @param[in] fileName the filename.
 */
void audioPipelineLink(int modus, char *fileName);

/**
 * @brief loop the audio pipeline, call this function to execute the audio pipeline loop.
 **/
void audioPipelineLoop(void);

/**
 * @brief stop the audio pipeline loop, call this function to unblock the loop.
 **/
void audioPipelinePause(void);

/**
 * @brief stop the audio pipeline, call this function after stopping the loop.
 **/
void audioPipelineTerminate(void);

/**
 * @brief destroy the audio pipeline, only call this function once.
 **/
void audioPipelineDestroy(void);

/**
 * @brief play an audio file from sdcard.
 **/
void audioPipelinePlaySD(char *fileName);

/**
 * @brief start playing radio.
 **/
void audioPipelinePlayRadio(void);

/**
 * @brief stop playing radio.
 **/
void audioPipelineStopRadio(void);

void audioAnalyseLoop(void *parameter);

void audioPipelineResetRadioChannel(void);

int audioPipelineGetVolume(void);
