#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "alarmTrigger.h"

// displays
#include "./displays/mainMenu.h"
#include "./displays/radioVolume.h"
#include "./displays/alarmSetMenu.h"
#include "./displays/alarmModus.h"
#include "./displays/channelMenu.h"
#include "./displays/alarmStudent.h"
#include "./displays/timeToSleepSetMenu.h"
#include "progressBar.h"
#include "lcd.h"

//#define testing
#ifndef testing
#include "lcd.h"
#endif

// List of menu items.
MENU_ITEM menu[] = {
    {
        MAIN_MENU,
        mainMenuDisplay,
        {NULL,
         NULL,
         mainMenuUpPressed,
         mainMenuDownPressed,
         mainMenuCenterPressed},
    },
    {
        RADIO_VOLUME,
        radioVolumeDisplay,
        {radioVolumeLeftPressed, radioVolumeRightPressed, radioVolumeUpPressed, radioVolumeDownPressed, NULL},
    },
    {
        ALARM_SET,
        alarmSetDisplay,
        {alarmLeftPressed, alarmRightPressed, alarmUpPressed, alarmDownPressed, alarmCenterPressed},
    },
    {ALARM_MODUS,
     alarmModusDisplay,
     {alarmModusLeftPressed,
      alarmModusRightPressed,
      alarmModusUpPressed,
      alarmModusDownPressed,
      alarmModusCenterPressed}},
    {ALARM_STUDENT,
     alarmStudentDisplay,
     {alarmStudentLeftPressed,
      alarmStudentRightPressed,
      alarmStudentUpPressed,
      alarmStudentDownPressed,
      alarmStudentCenterPressed}},
    {TIME_TO_SLEEP,
     ttsSetDisplay,
     {ttsLeftPressed,
      ttsRightPressed,
      ttsUpPressed,
      ttsDownPressed,
      ttsCenterPressed}},
    {
        CHANNEL_SELECTION,
        channelMenuSetDisplay,
        {channelMenuLeftPressed,
         channelMenuRightPressed,
         channelMenuUpPressed,
         channelMenuDownPressed,
         channelMenuCenterPressed},
    }};

static unsigned int currentMenuIndex = 0;
static unsigned int currentMenuId;

// #ifdef testing
void printMenuItem(DISPLAY_LINES lines)
{
    /* Display the item text */
    printf("=DISPLAY============\n");
    printf("%s\n", lines.line1);
    printf("%s\n", lines.line2);
    printf("%s\n", lines.line3);
    printf("%s\n", lines.line4);
    printf("====================\n\n");
}
// #endif

void menuUpdate(void)
{
    /* Lookup new current menu id in the array of menu items */
    currentMenuIndex = 0;
    while (menu[currentMenuIndex].id != currentMenuId)
    {
        currentMenuIndex += 1;
    }

    /* Display the menu item text */
    if (NULL != menu[currentMenuIndex].getText)
    {
        DISPLAY_LINES text = (*menu[currentMenuIndex].getText)();
#ifdef testing
        printMenuItem(text);
#else
        printMenuItem(text);
        lcdWriteScreen(text);
        if (currentMenuIndex == 0)
        {
            draw_progress_bar();
        }
#endif
    }
}

void menuHandleKey(int key)
{
    if (NULL != menu[currentMenuIndex].fpOnKey[key])
    {
        (*menu[currentMenuIndex].fpOnKey[key])();
    }

    menuUpdate();
}

void menuSetup()
{
#ifndef testing
    lcdInit();
    lcdBacklightOn();
#endif
    currentMenuId = menu[currentMenuIndex].id;
    if (NULL != menu[currentMenuIndex].getText)
    {
        DISPLAY_LINES text = (*menu[currentMenuIndex].getText)();
        if (currentMenuIndex == 0)
        {
            draw_progress_bar();
        }
#ifdef testing
        printMenuItem(text);
#else
        printMenuItem(text);
        lcdWriteScreen(text);
#endif
    }
}

void menuChange(int menu)
{
    currentMenuId = menu;
}
