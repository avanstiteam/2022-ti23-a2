#include "timeUpdate.h"
#include "./displays/menuGetters.h"
#include "./displays/dateGetters.h"
#include "./displays/timeSetters.h"

void updateCurrentTime()
{
    int minute = mainMenuGetMinutes();
    minute++;
    if (minute > 59)
    {
        minute = 0;
        int hour = mainMenuGetHours();
        hour++;
        if (hour > 23)
        {
            hour = 0;
            int day = mainMenuGetDay();
            day++;
            if (day > 31)
            {
                day = 1;
                int month = mainMenuGetMonth();
                month++;
                if (month > 12)
                {
                    month = 1;
                }
                timeSetMonths(month);
            }
            timeSetDays(day);
        }
        timeSetHours(hour);
    }
    timeSetMinutes(minute);
}