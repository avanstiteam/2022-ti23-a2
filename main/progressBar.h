#pragma once

/**
 * @brief This method draws a progress bar on the main menu screen.
 * The Progress bar is placed on the bottom row and will fill the entire width
 * of the lcd.
 * 
 * The progress bar will be visible if the alarm is within 60 minutes of
 * the current time.
 * 
 */
void draw_progress_bar();
