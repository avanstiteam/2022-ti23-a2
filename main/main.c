#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_system.h>

#include "config.h"

#include "menu.h"
#include "displays/mainMenu.h"
#include "audioPipeline.h"
// #include "button.h"
#include "audioFileSelector.h"
#include "a2_button.h"
#include "alarmTrigger.h"
#include "lcd.h"
#include "timeUpdate.h"
#include "weeklyAlarm.h"

void vTimerTask(void *pvParameters);

void left(){
    printf("left pressed\n");
    menuHandleKey(0);
}

void right(){
    printf("right pressed\n");
    menuHandleKey(1);
}

void up(){
    printf("up pressed\n");
    menuHandleKey(2);
}

void down(){
    printf("down pressed\n");
    menuHandleKey(3);
}

void center(){
    printf("center pressed\n");
    menuHandleKey(4);
}

Button_t buttons[] = {
    {
        .pressed = 0,
        .handled = 0,
        .handler = &left,
        .mask = (1 << 3),
        .gpio = PINB
    },
    {
        .pressed = 0,
        .handled = 0,
        .handler = &right,
        .mask = (1 << 5),
        .gpio = PINB
    },
    {
        .pressed = 0,
        .handled = 0,
        .handler = &up,
        .mask = (1 << 6),
        .gpio = PINB
    },
    {
        .pressed = 0,
        .handled = 0,
        .handler = &down,
        .mask = (1 << 7),
        .gpio = PINB
    },
    {
        .pressed = 0,
        .handled = 0,
        .handler = &center,
        .mask = (1 << 4),
        .gpio = PINB
    }
};


void app_main(void)
{
    audioPipelineInit();
    // soundAnalysis_init();
    setAlarmOverwriteMode(OVERWRITE);

    menuSetup();
    menuChange(0);
    // menuHandleKey(2);

    a2_gpio_init();
    a2_gpio_direction_set(0x00, PINA);
    a2_gpio_direction_set(0xff, PINB);
    a2_write(0xff, PINA);
    a2_button_init(buttons, 5);

    audioPipelineLink(2, NULL);
    xTaskCreate(vTimerTask, "update", 5000, NULL, 1, NULL);

    for (;;)
    {
        vTaskDelay(1000 / portTICK_RATE_MS);
    }

    a2_gpio_deinit();

    // speakCurrentTime();
}

void vTimerTask(void *pvParameters)
{
    for(;;)
    {
        updateCurrentTime();
        menuUpdate();
        alarmCheckTime();
        checkForNewDay();
        vTaskDelay(60000 / portTICK_RATE_MS);
    }
}
