#ifndef LCD_h
#define LCD_h

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
// #include "driver/gpio.h"
// #include "driver/i2c.h"
// #include "smbus.h"
#include "i2c-lcd1602.h"

#include "./displays/display.h"

i2c_lcd1602_info_t* lcd_info;

void lcdInit();
void lcdBacklightOn();
void lcdBacklightOff();
void lcdWriteScreen(DISPLAY_LINES lines);
void lcdWriteToLine(char *text, uint8_t lineNumber);
void lcdClear();

#endif
