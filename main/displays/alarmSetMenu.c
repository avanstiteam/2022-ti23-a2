#include <stdio.h>
#include <string.h>
#include "alarmSetMenu.h"
#include "alarmGetters.h"
#include "alarmSetters.h"
#include "../menu.h"
#include "../weeklyAlarm.h"

static alarmSetMenuState state = alarmSetSWITCHSCREENS;

static int hours = 7;
static int minutes = 0;

DISPLAY_LINES alarmSetDisplay(void)
{
    char linestr[20];
    if (minutes < 10)
    {
        sprintf(linestr, "%d:0%d", hours, minutes);
    } else
    {
        sprintf(linestr, "%d:%d", hours, minutes);
    }

    DISPLAY_LINES lines = {
        "Wekker>Tijd",
        " ",
        " ",
        " ",
    };
    strcpy(lines.line3, linestr);
    return lines;
}

int alarmGetHours()
{
    return hours;
}

int alarmGetMinutes()
{
    return minutes;
}

void alarmSetHours(int hour)
{
    hours = hour;
}

void alarmSetMinutes(int minute)
{
    minutes = minute;
}

void alarmLeftPressed()
{
    if (state == alarmSetSWITCHSCREENS)
    {
        alarmLeaveMenu(TIME_TO_SLEEP);
    }
}

void alarmRightPressed()
{
    if (state == alarmSetSWITCHSCREENS)
    {
        alarmLeaveMenu(ALARM_MODUS);
    }
}

void alarmUpPressed()
{
    switch (state){
        case alarmSetSWITCHSCREENS:
            alarmLeaveMenu(CHANNEL_SELECTION);
            break;
        case alarmSetEDITHOURS:
            hours++;
            if (hours > 23)
            {
                hours = 0;
            }
            break;
        case alarmSetEDITMINUTES:
            minutes++;
            if (minutes > 59)
            {
                hours = 0;
            }
            break;
    }
}

void alarmDownPressed()
{
    switch (state){
        case alarmSetSWITCHSCREENS:
            alarmLeaveMenu(MAIN_MENU);
            break;
        case alarmSetEDITHOURS:
            hours--;
            if (hours < 0)
            {
                hours = 23;
            }
            break;
        case alarmSetEDITMINUTES:
            minutes--;
            if (minutes < 0)
            {
                minutes = 59;
            }
            break;
    }
}

void alarmCenterPressed()
{
    if (state == alarmSetEDITMINUTES)
    {
        state = alarmSetSWITCHSCREENS;
    } else {
        state++;
    }
}

void alarmLeaveMenu(int menu)
{
    addAlarmTime(hours, minutes);
    menuChange(menu);
}
