#pragma once

#include "display.h"

DISPLAY_LINES alarmStudentDisplay(void);

void alarmStudentLeftPressed();

void alarmStudentRightPressed();

void alarmStudentUpPressed();

void alarmStudentDownPressed();

void alarmStudentCenterPressed();

void studentAlarmCheck();

void studentPauseCheck();

/**
 * @brief Set the studentAlarmStart object
 * 
 * @param min Amount of minutes 
 * @param hour Amount of hours
 */
void setStudentAlarmStart(int min, int hour);

typedef enum{
    alarmStudentSWITCHSCREENS = 0,
    alarmStudentEDITHOURS = 1,
    alarmStudentEDITMINUTES = 2,
    alarmStudentEDITTIMES = 3,
} alarmStudentState;