#pragma once

#include "display.h"

/**
 * @brief ttsSetDisplay makes the text to display on the lcd
 * @returns DISPLAY_LINES with correct text
 **/
DISPLAY_LINES ttsSetDisplay(void);

/**
 * @brief ttsLeftPressed handles the event when left is pressed.
 **/
void ttsLeftPressed(void);

/**
 * @brief ttsRightPressed handles the event when right is pressed.
 **/
void ttsRightPressed(void);

/**
 * @brief ttsUpPressed handles the event when up is pressed.
 **/
void ttsUpPressed(void);

/**
 * @brief ttsDownPressed handles the event when down is pressed.
 **/
void ttsDownPressed(void);

/**
 * @brief ttsCenterPressed handles the event when center is pressed.
 **/
void ttsCenterPressed(void);

/**
 * @brief ttsSetMenuState manages the state of the menu.
 * 0 = switchscreens
 * 1 = edithours
 * 2 = editminutes
 **/
typedef enum{
    ttsSetSWITCHSCREENS = 0,
    ttsSetEDITHOURS = 1,
    ttsSetEDITMINUTES = 2,
    ttsSetEDITACTIVE = 3
} ttsSetMenuState;
