#include <stdio.h>
#include <string.h>
#include "alarmStudent.h"
#include "alarmGetters.h"
#include "../menu.h"
#include "menuGetters.h"
#include "../audioFileSelector.h"

static alarmStudentState state = alarmStudentSWITCHSCREENS;

// Variables needed for Student Alarm
static int studentAlarmHours = 0;
static int studentAlarmMinutes = 0;
static int studentAlarmStartHours = 0;
static int studentAlarmStartMinutes = 0;
static int studentAlarmRunning = 0;
static int studentAlarmTimes = 0;

//Variables needed for Student Pause
static int studentPauseMinutes = 10; // Amount of minutes break inbetween study sessions
static int studentPauseActive = 0;
static int studentPauseStartHours;
static int studentPauseStartMinutes;
static int timesRepeated = 0;

DISPLAY_LINES alarmStudentDisplay(void)
{
    char linestr[20];
    if (studentAlarmMinutes < 10)
    {
        sprintf(linestr, "%d:0%d %d", studentAlarmHours, studentAlarmMinutes, studentAlarmTimes);
    }
    else
    {
        sprintf(linestr, "%d:%d %d", studentAlarmHours, studentAlarmMinutes, studentAlarmTimes);
    }

    DISPLAY_LINES lines = {
        "Wekker>Student",
        " ",
        " ",
        " ",
    };
    strcpy(lines.line3, linestr);
    return lines;
}

int alarmStudentGetHours()
{
    return studentAlarmHours;
}

int alarmStudentGetMinutes()
{
    return studentAlarmMinutes;
}

int alarmStudentGetStartHours()
{
    return studentAlarmStartHours;
}

int alarmStudentGetStartMinutes()
{
    return studentAlarmStartMinutes;
}

int alarmStudentGetTimes()
{
    return studentAlarmTimes;
}

int alarmStudentIsActive()
{
    return studentAlarmRunning;
}

int pauseStudentIsActive()
{
    return studentPauseActive;
}

/**
 * @brief Left Button pressed action
 * 
 */
void alarmStudentLeftPressed()
{
    if (state == alarmStudentSWITCHSCREENS)
    {
        menuChange(ALARM_MODUS);
    }
}

/**
 * @brief Right Button pressed action
 * 
 */
void alarmStudentRightPressed()
{
    if (state == alarmStudentSWITCHSCREENS)
    {
        menuChange(TIME_TO_SLEEP);
    }
}

/**
 * @brief Up Button pressed action
 * 
 */
void alarmStudentUpPressed()
{
    switch (state)
    {
    case alarmStudentSWITCHSCREENS:
        menuChange(CHANNEL_SELECTION);
        break;
    case alarmStudentEDITHOURS:
        studentAlarmHours++;
        if (studentAlarmHours > 23)
        {
            studentAlarmHours = 0;
        }
        break;
    case alarmStudentEDITMINUTES:
        studentAlarmMinutes++;
        if (studentAlarmMinutes > 59)
        {
            studentAlarmHours = 0;
        }
        break;
    case alarmStudentEDITTIMES:
        studentAlarmTimes++;
        if (studentAlarmTimes > 99)
        {
            studentAlarmTimes = 0;
        }
    }
}

/**
 * @brief Down Button pressed action
 * 
 */
void alarmStudentDownPressed()
{
    switch (state)
    {
    case alarmStudentSWITCHSCREENS:
        menuChange(MAIN_MENU);
        break;
    case alarmStudentEDITHOURS:
        studentAlarmHours--;
        if (studentAlarmHours < 0)
        {
            studentAlarmHours = 23;
        }
        break;
    case alarmStudentEDITMINUTES:
        studentAlarmMinutes--;
        if (studentAlarmMinutes < 0)
        {
            studentAlarmMinutes = 59;
        }
        break;
    case alarmStudentEDITTIMES:
        studentAlarmTimes--;
        if (studentAlarmTimes < 0)
        {
            studentAlarmTimes = 99;
        }
    }
}

/**
 * @brief Center Button pressed action
 * 
 */
void alarmStudentCenterPressed()
{
    if (state == alarmStudentEDITTIMES)
    {
        studentAlarmRunning = 1;
        studentAlarmStartMinutes = mainMenuGetMinutes();
        studentAlarmStartHours = mainMenuGetHours();
        timesRepeated = 0;
        state = alarmStudentSWITCHSCREENS;
    }
    else
    {
        state++;
    }
}

void setStudentAlarmStart(int min, int hour)
{
    studentAlarmStartHours = hour;
    studentAlarmStartMinutes = min;
}

/**
 * @brief Checks if the Student Alarm has to go off
 * 
 */
void studentAlarmCheck()
{
    int currentHours = mainMenuGetHours();
    int currentMinutes = mainMenuGetMinutes();
    if (studentAlarmStartMinutes + studentAlarmMinutes == 60)
    {
        studentAlarmHours++;
        studentAlarmMinutes = 0;
    }

    if (studentAlarmStartMinutes + studentAlarmMinutes > 60)
    {
        studentAlarmHours += (studentAlarmMinutes / 60) - 1;
        studentAlarmMinutes = studentAlarmMinutes % 60;
    }

    if (currentHours == studentAlarmStartHours + studentAlarmHours && currentMinutes == studentAlarmStartMinutes + studentAlarmMinutes)
    {
        printf("STUDENT ALARM!\n");
        speakStudentAlarm();
        studentPauseActive = 1;
        studentPauseStartHours = currentHours;
        studentPauseStartMinutes = currentMinutes;
    }
}

/**
 * @brief Checks if the Student Pause has to go off
 * 
 */
void studentPauseCheck()
{
    int currentHours = mainMenuGetHours();
    int currentMinutes = mainMenuGetMinutes();

    if (studentPauseStartMinutes + studentPauseMinutes == 60)
    {
        studentPauseStartHours++;
        studentPauseMinutes = 0;
    }

    if (studentPauseStartMinutes + studentPauseMinutes > 60)
    {
        studentPauseStartHours++;
        studentPauseMinutes = studentPauseMinutes % 60;
    }

    if (currentHours == studentPauseStartHours && currentMinutes == studentPauseStartMinutes + studentPauseMinutes)
    {
        printf("STUDENT START STUDYING!\n");
        speakStudentBreakOver();
        studentPauseActive = 0;

        if (timesRepeated < studentAlarmTimes)
        {
            timesRepeated++;
            setStudentAlarmStart(currentMinutes, currentHours);
        }
        else
        {
            studentAlarmRunning = 0;
        }
    }
}
