#pragma once

#include "display.h"

DISPLAY_LINES alarmSetDisplay(void);

void alarmLeftPressed();

void alarmRightPressed();

void alarmUpPressed();

void alarmDownPressed();

void alarmCenterPressed();

/**
 * @brief Wrapper around the menuChange() method. Saves the currently set alarm.
 * 
 * @param menu The menu to navigate to.
 */
void alarmLeaveMenu(int menu);

typedef enum{
    alarmSetSWITCHSCREENS = 0,
    alarmSetEDITHOURS = 1,
    alarmSetEDITMINUTES = 2
} alarmSetMenuState;