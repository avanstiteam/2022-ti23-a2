#pragma once

#include "display.h"

DISPLAY_LINES radioVolumeDisplay(void);

void radioVolumeLeftPressed();

void radioVolumeRightPressed();

void radioVolumeUpPressed(void);

void radioVolumeDownPressed(void);
