#pragma once

#include "display.h"

DISPLAY_LINES mainMenuDisplay(void);

void mainMenuUpPressed(void);

void mainMenuDownPressed(void);

void mainMenuCenterPressed(void);

typedef enum{
    mainMenuSWITCHSCREENS = 0,
    mainMenuEDITDAYS = 1,
    mainMenuEDITMONTHS = 2,
    mainMenuEDITHOURS = 3,
    mainMenuEDITMINUTES = 4
} mainMenuState;