#pragma once

/**
 * @brief ttsGetHours returns the hours.
 **/
int ttsGetHours(void);

/**
 * @brief ttsGetMinutes returns the minutes.
 **/
int ttsGetMinutes(void);

/**
 * @brief ttsGetActive returns the isActive.
 **/
int ttsGetActive(void);
