#pragma once

#include "display.h"

DISPLAY_LINES alarmModusDisplay(void);

void alarmModusLeftPressed();

void alarmModusRightPressed();

void alarmModusUpPressed();

void alarmModusDownPressed();

void alarmModusCenterPressed();

int alarmGetModus();

typedef enum{
    alarmModusSWITCHSCREENS = 0,
    alarmModusEDITMODUS = 1
} alarmModusMenuState;