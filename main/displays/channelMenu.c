#include <stdio.h>
#include <string.h>
#include "channelMenu.h"
#include "channelGetter.h"
#include "../audioPipeline.h"
#include "../menu.h"

static char *channels[CHANNELAMOUNT] = {
    "https://dl.espressif.com/dl/audio/ff-16b-2c-44100hz.mp3",
    "https://streams.pinguinradio.com/Aardschok192.mp3",
    "https://playerservices.streamtheworld.com/api/livestream-redirect/RADIO_2_ANTWERP_128.mp3",
    "http://stream4.nsupdate.info:8100/biglh.mp3",
};

static char *channelNames[CHANNELAMOUNT] = {
    "esspressif",
    "pinguin",
    "Antwerpen",
    "Big L",
};

static int channelIndex = 0;
static channelMenuState channelState = channelSWITCHSCREENS;

DISPLAY_LINES channelMenuSetDisplay(void)
{
    DISPLAY_LINES lines = {
        "Radio>Zender",
        "",
        "",
        ""};
    strcpy(lines.line3, channelNames[channelIndex]);
    return lines;
}

void channelMenuLeftPressed(void)
{
    if (channelState == channelSWITCHSCREENS)
    {
        menuChange(RADIO_VOLUME);
    }
}

void channelMenuRightPressed(void)
{
    if (channelState == channelSWITCHSCREENS)
    {
        menuChange(RADIO_VOLUME);
    }
}

void channelMenuUpPressed(void)
{
    if (channelState == channelSWITCHSCREENS)
    {
        menuChange(MAIN_MENU);
    }
    else
    {
        channelIndex++;
        if (channelIndex == CHANNELAMOUNT)
        {
            channelIndex = 0;
        }
        audioPipelineResetRadioChannel();
    }
}

void channelMenuDownPressed(void)
{
    if (channelState == channelSWITCHSCREENS)
    {
        menuChange(ALARM_SET);
    }
    else
    {
        channelIndex--;
        if (channelIndex < 0)
        {
            channelIndex = CHANNELAMOUNT - 1;
        }
        audioPipelineResetRadioChannel();
    }
}

void channelMenuCenterPressed(void)
{
    if (channelState == channelSWITCHSCREENS)
    {
        channelState = channelCHANNEL;
    }
    else
    {
        channelState = channelSWITCHSCREENS;
    }
}

char *channelMenuGetChannel(void)
{
    return (channels[channelIndex]);
}
