#pragma once

int alarmStudentGetHours();

int alarmStudentGetMinutes();

int alarmStudentGetStartMinutes();

int alarmStudentGetStartHours();

int alarmStudentIsActive();

int alarmStudentGetTimes();

int pauseStudentIsActive();
