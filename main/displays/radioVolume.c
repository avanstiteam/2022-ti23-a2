#include "radioVolume.h"
#include "../audioPipeline.h"
#include "../menu.h"
#include "./display.h"
#include <stdio.h>

DISPLAY_LINES radioVolumeDisplay(void)
{
    DISPLAY_LINES lines = {
        "Radio>volume",
        " ",
        " ",
        " ",
    };
    sprintf(lines.line3, "%d", audioPipelineGetVolume());
    return lines;
}

void radioVolumeLeftPressed()
{
    menuChange(CHANNEL_SELECTION);
}

void radioVolumeRightPressed()
{
    menuChange(CHANNEL_SELECTION);
}

void radioVolumeUpPressed(void)
{
    menuChange(MAIN_MENU);
}

void radioVolumeDownPressed(void)
{
    menuChange(ALARM_SET);
}
