#pragma once

typedef struct
{
    char line1[20];
    char line2[20];
    char line3[20];
    char line4[20];
} DISPLAY_LINES;

#define MAIN_MENU 0
#define ALARM_SET 1
#define RADIO_VOLUME 2
#define CHANNEL_SELECTION 3
#define ALARM_MODUS 4
#define ALARM_STUDENT 5
#define TIME_TO_SLEEP 6
