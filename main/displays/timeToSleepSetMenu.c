#include <stdio.h>
#include <string.h>
#include "timeToSleepSetMenu.h"
#include "timeToSleepGetters.h"
#include "../menu.h"

static ttsSetMenuState ttsState = ttsSetSWITCHSCREENS;

static int hours = 22;
static int minutes = 2;
static int isActive = 1;

/**
 * @brief ttsSetDisplay makes the text to display on the lcd
 * @returns DISPLAY_LINES with correct text
 **/
DISPLAY_LINES ttsSetDisplay(void)
{
    DISPLAY_LINES lines = {
        "Wekker>Slaaptijd",
        " ",
        " ",
        " ",
    };

    char linestr[20];
    sprintf(linestr, "%02d:%02d", hours, minutes);

    strcpy(lines.line3, linestr);

    if (isActive)
    {
        strcpy(lines.line2, "Aan");
    }
    else
    {
        strcpy(lines.line2, "Uit");
    }
    
    return lines;
}

/**
 * @brief ttsGetHours returns the hours.
 **/
int ttsGetHours(void)
{
    return hours;
}

/**
 * @brief ttsGetMinutes returns the minutes.
 **/
int ttsGetMinutes(void)
{
    return minutes;
}

/**
 * @brief ttsGetActive returns the isActive.
 **/
int ttsGetActive(void)
{
    return isActive;
}

/**
 * @brief ttsLeftPressed handles the event when left is pressed.
 **/
void ttsLeftPressed(void)
{
    if (ttsState == ttsSetSWITCHSCREENS)
    {
        // Change menu to ALARM_STUDENT.
        menuChange(ALARM_STUDENT);
    }
}

/**
 * @brief ttsRightPressed handles the event when right is pressed.
 **/
void ttsRightPressed(void)
{
    if (ttsState == ttsSetSWITCHSCREENS)
    {
        // Change menu to ALARM_SET.
        menuChange(ALARM_SET);
    }
}

/**
 * @brief ttsUpPressed handles the event when up is pressed.
 **/
void ttsUpPressed(void)
{
    switch (ttsState)
    {
    case ttsSetSWITCHSCREENS:
        // Change menu to CHANNEL_SELECTION.
        menuChange(CHANNEL_SELECTION);
        break;
    case ttsSetEDITHOURS:
        // Increment hours.
        hours++;
        if (hours > 23)
        {
            hours = 0;
        }
        break;
    case ttsSetEDITMINUTES:
        // Increment minutes.
        minutes++;
        if (minutes > 59)
        {
            hours = 0;
        }
        break;
    case ttsSetEDITACTIVE:
        if (isActive)
        {
            isActive = 0;
        }
        else
        {
            isActive = 1;
        }
        break;
    }
}

/**
 * @brief ttsDownPressed handles the event when down is pressed.
 **/
void ttsDownPressed(void)
{
    switch (ttsState)
    {
    case ttsSetSWITCHSCREENS:
        // Change menu to MAIN_MENU.
        menuChange(MAIN_MENU);
        break;
    case ttsSetEDITHOURS:
        // Decrement hours.
        hours--;
        if (hours < 0)
        {
            hours = 23;
        }
        break;
    case ttsSetEDITMINUTES:
        // Decrement minutes.
        minutes--;
        if (minutes < 0)
        {
            minutes = 59;
        }
        break;
    case ttsSetEDITACTIVE:
        if (isActive)
        {
            isActive = 0;
        }
        else
        {
            isActive = 1;
        }
        break;
    }
}

/**
 * @brief ttsCenterPressed handles the event when center is pressed.
 **/
void ttsCenterPressed(void)
{
    if (ttsState == ttsSetEDITACTIVE)
    {
        // Change state back to first state.
        ttsState = ttsSetSWITCHSCREENS;
    }
    else
    {
        // Increment state.
        ttsState++;
    }
}
