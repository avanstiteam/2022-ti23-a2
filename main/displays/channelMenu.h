#pragma once

#define CHANNELAMOUNT 4

#include "display.h"

DISPLAY_LINES channelMenuSetDisplay(void);

void channelMenuLeftPressed(void);

void channelMenuRightPressed(void);

void channelMenuUpPressed(void);

void channelMenuDownPressed(void);

void channelMenuCenterPressed(void);

void resetChannel(void);

typedef enum{
    channelSWITCHSCREENS = 0,
    channelCHANNEL = 1
} channelMenuState;
