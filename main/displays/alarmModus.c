#include <stdio.h>
#include <string.h>
#include "alarmModus.h"
#include "../menu.h"

static alarmModusMenuState modusState = alarmModusSWITCHSCREENS;

static int currentMode = 1;
static char* modus[3] = {
    "Radio",
    "Alarm",
    "Uit"
};

DISPLAY_LINES alarmModusDisplay(void)
{
    DISPLAY_LINES lines = {
        "Wekker>Modus",
        " ",
        " ",
        " ",
    };
    strcpy(lines.line3, modus[currentMode]);
    return lines;
}

void alarmModusLeftPressed()
{
    if (modusState == alarmModusSWITCHSCREENS)
    {
        menuChange(ALARM_SET);
    }
}

void alarmModusRightPressed()
{
    if (modusState == alarmModusSWITCHSCREENS)
    {
        menuChange(ALARM_STUDENT);
    }
}

void alarmModusUpPressed()
{
    if (modusState == alarmModusSWITCHSCREENS)
    {
        menuChange(CHANNEL_SELECTION);
    } else
    {
        currentMode++;
        if (currentMode > 2)
        {
            currentMode = 0;
        }
    }
}

void alarmModusDownPressed()
{
    if (modusState == alarmModusSWITCHSCREENS)
    {
        menuChange(MAIN_MENU);
    } else
    {
        currentMode--;
        if (currentMode < 0)
        {
            currentMode = 2;
        }
    }
}

void alarmModusCenterPressed()
{
    if (modusState == alarmModusSWITCHSCREENS)
    {
        modusState = alarmModusEDITMODUS;
    } else
    {
        modusState = alarmModusSWITCHSCREENS;
    }
}

int alarmGetModus()
{
    return currentMode;
}