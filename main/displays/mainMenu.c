#include <stdio.h>
#include <string.h>
#include <time.h>
#include "mainMenu.h"
#include "../menu.h"
#include "../weeklyAlarm.h"
#include "menuGetters.h"
#include "dateGetters.h"
#include "timeSetters.h"
#include "../progressBar.h"

static mainMenuState state = mainMenuSWITCHSCREENS;

static int day = 1;
static int month = 1;
static int hour = 0;
static int minute = 0;

static char* weekdays[] =
{
    "Mon",
    "Teu",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
};

DISPLAY_LINES mainMenuDisplay(void)
{
    char timestr[20];

    int index = getCurrentDayOfWeek();

    if (minute < 10)
    {
        sprintf(timestr, "%s %d-%d %d:0%d", weekdays[index], day, month, hour, minute);
    }
    else
    {
        sprintf(timestr, "%s %d-%d %d:%d", weekdays[index], day, month, hour, minute);
    }
    

    DISPLAY_LINES lines = {
        "",
        "",
        "",
        "",
    };
    strcpy(lines.line1, timestr);
    
    return lines;
}

int mainMenuGetHours()
{
    return hour;
}

int mainMenuGetMinutes()
{
    return minute;
}

int mainMenuGetMonth()
{
    return month;
}

int mainMenuGetDay()
{
    return day;
}

void mainMenuUpPressed(void)
{
    switch (state){
        case mainMenuSWITCHSCREENS:
            menuChange(ALARM_SET);
            break;
        case mainMenuEDITDAYS:
            day++;
            if (day > 31)
            {
                day = 1;
            }
            setTodaysAlarmUsingDate(month, day);
            break;
        case mainMenuEDITMONTHS:
            month++;
            if (month > 12)
            {
                month = 1;
            }
            setTodaysAlarmUsingDate(month, day);
            break;
        case mainMenuEDITHOURS:
            hour++;
            if (hour > 23)
            {
                hour = 0;
            }
            break;
        case mainMenuEDITMINUTES:
            minute++;
            if (minute > 59)
            {
                minute = 0;
            }
            break;
    }
}

void mainMenuDownPressed(void)
{
    switch (state){
        case mainMenuSWITCHSCREENS:
            menuChange(CHANNEL_SELECTION);
            break;
        case mainMenuEDITDAYS:
            day--;
            if (day < 1)
            {
                day = 31;
            }
            setTodaysAlarmUsingDate(month, day);
            break;
        case mainMenuEDITMONTHS:
            month--;
            if (month < 1)
            {
                month = 12;
            }
            setTodaysAlarmUsingDate(month, day);
            break;
        case mainMenuEDITHOURS:
            hour--;
            if (hour < 0)
            {
                hour = 23;
            }
            break;
        case mainMenuEDITMINUTES:
            minute--;
            if (minute < 0)
            {
                minute = 59;
            }
            break;
    }
}

void mainMenuCenterPressed(void)
{
    if (state == mainMenuEDITMINUTES)
    {
        state = mainMenuSWITCHSCREENS;
    } else {
        state++;
    }
}

void timeSetMonths(int months)
{
    month = months;
    setTodaysAlarmUsingDate(month, day);
}

void timeSetDays(int days)
{
    day = days;
    setTodaysAlarmUsingDate(month, day);
}

void timeSetHours(int hours)
{
    hour = hours;
}

void timeSetMinutes(int minutes)
{
    minute = minutes;
}
