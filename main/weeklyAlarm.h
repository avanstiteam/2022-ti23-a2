#pragma once

typedef enum
{
    OVERWRITE,
    NO_OVERWRITE
} overwriteType;

/**
 * @brief Add an alarm time for the current day of the week.
 * 
 * @param hours target hour for the alarm
 * @param minutes target minute for the alarm
 */
void addAlarmTime(int hours, int minutes);

/**
 * @brief Set todays alarm for a day of the week.
 * 
 * @param month current month
 * @param day current day
 */
void setTodaysAlarmUsingDate(int month, int day);

/**
 * @brief Set to either overwrite alarm times or to keep the old alarmdata.
 * 
 * @param overwritter the mode to switch to. 
 */
void setAlarmOverwriteMode(overwriteType overwritter);

/**
 * @brief Run every minute to check of the next alarm needs to be set.
 * 
 */
void checkForNewDay(void);

/**
 * @brief Get the current day of the week;
 * 
 * @return int 
 */
int getCurrentDayOfWeek();
