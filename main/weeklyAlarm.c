#include "displays/dateGetters.h"
#include "displays/alarmSetters.h"
#include "displays/menuGetters.h"
#include "weeklyAlarm.h"
#include "esp_log.h"

#define NOT_EMPTY 0
#define EMPTY 1

#define WEEKDAYS 7

/**
 * @brief Set todays alarm for the current day of the week.
 * 
 */
static void setTodaysAlarm(void);


static const char *TAG = "weekly alarm";

//Used to save the alarm times.
typedef struct {
    int empty;
    int hours;
    int minutes;
} alarmTime;

//Stores the alarms for day 0 to 6.
static alarmTime weeklyAlarms[WEEKDAYS] = {
    {.empty = EMPTY},
    {.empty = EMPTY},
    {.empty = EMPTY},
    {.empty = EMPTY},
    {.empty = EMPTY},
    {.empty = EMPTY},
    {.empty = EMPTY}
};

static overwriteType overwriteMode;

void addAlarmTime(int hour, int minute)
{
    int month = mainMenuGetMonth();
    int day = mainMenuGetDay();

    //All months have 31 days.
    int index = (month * 31 + day) % WEEKDAYS;
    ESP_LOGI(TAG, "Wrote alarm to day: %d.", index);

    if (weeklyAlarms[index].empty == EMPTY)
    {
        alarmTime newTime;

        newTime.empty = NOT_EMPTY;
        newTime.hours = hour;
        newTime.minutes = minute;

        weeklyAlarms[index] = newTime;

        ESP_LOGI(TAG, "Alarm: %d:%02d.", hour, minute);
    }
    else
    {
        if (overwriteMode == OVERWRITE)
        {
            weeklyAlarms[index].hours = hour;
            weeklyAlarms[index].minutes = minute;

            ESP_LOGI(TAG, "Alarm: %d:%02d", hour, minute);
        } else
        {
            ESP_LOGI(TAG, "Could not set alarm, one already exists for this day.");
        }
    }
}

static void setTodaysAlarm()
{
    int month = mainMenuGetMonth();
    int day = mainMenuGetDay();

    setTodaysAlarmUsingDate(month, day);
}

void setTodaysAlarmUsingDate(int month, int day)
{
    ESP_LOGI(TAG, "current date: %d-%d.", day, month);

    //All months have 31 days.
    int index = (month * 31 + day) % WEEKDAYS;

    if (weeklyAlarms[index].empty == NOT_EMPTY)
    {
        alarmTime todaysAlarm = weeklyAlarms[index];

        int hour = todaysAlarm.hours;
        int minute = todaysAlarm.minutes;

        alarmSetHours(hour);
        alarmSetMinutes(minute);

        ESP_LOGI(TAG, "Wrote today's alarm to %d:%02d.", hour, minute);
    }
}

void setAlarmOverwriteMode(overwriteType overwritter)
{
    overwriteMode = overwritter;
}

void checkForNewDay()
{
    int currentHours = mainMenuGetHours();
    int currentMinutes = mainMenuGetMinutes();

    if (currentHours == 0 && currentMinutes == 0)
    {
        setTodaysAlarm();
    }
}

int getCurrentDayOfWeek()
{
    int month = mainMenuGetMonth();
    int day = mainMenuGetDay();
    return (month * 31 + day) % WEEKDAYS;
}
