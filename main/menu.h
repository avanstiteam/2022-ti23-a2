#pragma once

#include "./displays/display.h"

#define LCD_MAX_LINES 4
#define MAX_MENU_KEY 5

typedef struct
{
    unsigned int id;                     /* ID for this item */
    DISPLAY_LINES (*getText)(void);      /* Text for this item */
    void (*fpOnKey[MAX_MENU_KEY])(void); /* Function pointer for each key (left, right, up, down, center) */
} MENU_ITEM;

void menuHandleKey(int key);
void menuSetup();
void menuChange(int menu);
void menuUpdate(void);
