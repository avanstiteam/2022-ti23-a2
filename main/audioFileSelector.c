#include "audioFileSelector.h"
#include "stdio.h"
#include "audioPipeline.h"
#include "displays/menuGetters.h"

void speakCurrentTime(void)
{
    // Start of sentence.
    audioPipelinePlaySD("hetis");

    // Say the current hour.
    if(mainMenuGetHours() == 0)
    {// If time is 00:xx say 12.
        audioPipelinePlaySD("12");
    }
    else
    {
        char hour[3];
        sprintf(hour, "%d", mainMenuGetHours());
        audioPipelinePlaySD(hour);
    }
    
    // Say the word hour.
    audioPipelinePlaySD("uur");
    // Say the current minute if applicable.
    if(mainMenuGetMinutes() != 0)
    {
        char minute[3];
        sprintf(minute, "%d", mainMenuGetMinutes());
        audioPipelinePlaySD(minute);
    }
    audioPipelineLink(2, NULL);
}

void speakStudentAlarm(void)
{
    audioPipelinePlaySD("pau");
    audioPipelineLink(2, NULL);
}

void speakStudentBreakOver(void)
{
    audioPipelinePlaySD("stu");
    audioPipelineLink(2, NULL);
}

void speakTimeToSleep(void)
{
    audioPipelinePlaySD("tts");
    audioPipelineLink(2, NULL);
}
