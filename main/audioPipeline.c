#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "mp3_decoder.h"
#include "esp_peripherals.h"
#include "periph_wifi.h"
#include "board.h"
// #include "tcpip_adapter.h"
#include "fatfs_stream.h"
#include "periph_sdcard.h"
#include "displays/channelGetter.h"
#include <math.h>
#include "goertzelFilter.h"
#include "filter_resample.h"
#include "lcd.h"
#include "periph_touch.h"
#include "input_key_service.h"

#include "credentials.h"
#include "audioPipeline.h"

static const char *TAG = "audioPipeline";
audio_pipeline_handle_t pipeline;
audio_element_handle_t fatfs_stream_reader, http_stream_reader, i2s_stream_writer, i2s_stream_reader, mp3_decoder,
    resample_filter, raw_reader;
esp_periph_set_handle_t set;
audio_event_iface_handle_t evt;
audio_board_handle_t board_handle;
periph_service_handle_t input_ser;
static int SDIsRunning;

int volume;

void audioAnalyseLoop(void *parameter);
static esp_err_t input_key_service_cb(periph_service_handle_t handle, periph_service_event_t *evt, void *ctx);

static const int GOERTZEL_DETECT_FREQS[] = {
    1175,
    1319,
    1478,
    1589,
};

#define GOERTZEL_NR_FREQS ((sizeof GOERTZEL_DETECT_FREQS) / (sizeof GOERTZEL_DETECT_FREQS[0]))
#define GOERTZEL_SAMPLE_RATE_HZ 8000                                                       // Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 100                                                       // Block length in [ms]
#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000) // Buffer length in samples
#define GOERTZEL_DETECTION_THRESHOLD 53.0f                                                 // Detect a tone when log magnitude is above this value
#define AUDIO_SAMPLE_RATE 48000

goertzel_filter_cfg_t filters_cfg[GOERTZEL_NR_FREQS];
goertzel_filter_data_t filters_data[GOERTZEL_NR_FREQS]; // Audio capture sample rate [Hz]
static int foundFreq = 0;

/**
 * Determine if a frequency was detected or not, based on the magnitude that the
 * Goertzel filter calculated
 * Use a logarithm for the magnitude
 */
static void detect_freq(int target_freq, float magnitude)
{
    float logMagnitude = 10.0f * log10f(magnitude);
    if (logMagnitude > GOERTZEL_DETECTION_THRESHOLD)
    {
        foundFreq = 1;
        ESP_LOGI(TAG, "Detection at frequency %d Hz (magnitude %.2f, log magnitude %.2f)", target_freq, magnitude, logMagnitude);
    }
}

void audioPipelineInit(void)
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES)
    {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    esp_netif_init();

    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set(TAG, ESP_LOG_DEBUG);

    ESP_LOGI(TAG, "[1.1] Start audio codec chip");
    board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);

    audio_hal_get_volume(board_handle->audio_hal, &volume);

    ESP_LOGI(TAG, "[1.2] Mount sdcard");
    // Initialize peripherals management
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    set = esp_periph_set_init(&periph_cfg);


    ESP_LOGI(TAG, "Initialize keys on board");
    audio_board_key_init(set);

    ESP_LOGI(TAG, "Create and start input key service");
    input_key_service_info_t input_key_info[] = INPUT_KEY_DEFAULT_INFO();
    input_key_service_cfg_t input_cfg = INPUT_KEY_SERVICE_DEFAULT_CONFIG();
    input_cfg.handle = set;
    input_ser = input_key_service_create(&input_cfg);
    input_key_service_add_key(input_ser, input_key_info, INPUT_KEY_NUM);
    periph_service_set_callback(input_ser, input_key_service_cb, (void *)board_handle);

    // Initialize SD Card peripheral
    audio_board_sdcard_init(set, SD_MODE_1_LINE);

    ESP_LOGI(TAG, "[1.3] Start and wait for Wi-Fi network");
    periph_wifi_cfg_t wifi_cfg = {
        .ssid = SSID,
        .password = PASSWORD,
    };
    esp_periph_handle_t wifi_handle = periph_wifi_init(&wifi_cfg);
    esp_periph_start(set, wifi_handle);
    periph_wifi_wait_for_connected(wifi_handle, portMAX_DELAY);

    ESP_LOGI(TAG, "[2.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    ESP_LOGI(TAG, "[2.1] Create fatfs stream to read data from sdcard");
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);

    ESP_LOGI(TAG, "[2.2] Create http stream to read data");
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_stream_reader = http_stream_init(&http_cfg);

    ESP_LOGI(TAG, "[2.3] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    i2s_stream_writer = i2s_stream_init(&i2s_cfg);
    ESP_LOGI(TAG, "[2.3] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg2 = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg2.type = AUDIO_STREAM_READER;
    i2s_stream_reader = i2s_stream_init(&i2s_cfg2);

    ESP_LOGI(TAG, "[2.4] Create mp3 decoder to decode mp3 file");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    mp3_decoder = mp3_decoder_init(&mp3_cfg);

    ESP_LOGI(TAG, "[2.6] Create resample_filter");
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = AUDIO_SAMPLE_RATE;
    rsp_cfg.src_ch = 2;
    rsp_cfg.dest_rate = GOERTZEL_SAMPLE_RATE_HZ;
    rsp_cfg.dest_ch = 1;
    resample_filter = rsp_filter_init(&rsp_cfg);

    ESP_LOGI(TAG, "[2.7] Create raw_reader");
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    raw_reader = raw_stream_init(&raw_cfg);

    ESP_LOGI(TAG, "[2.8] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, fatfs_stream_reader, "file");
    audio_pipeline_register(pipeline, http_stream_reader, "http");
    audio_pipeline_register(pipeline, mp3_decoder, "mp3");
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2sw");
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2sr");
    audio_pipeline_register(pipeline, resample_filter, "rsp_filter");
    audio_pipeline_register(pipeline, raw_reader, "raw");
}

void audioPipelineStopAndWait()
{
    ESP_LOGI(TAG, "[ * ] Stop audio_pipeline");
    audio_pipeline_pause(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
}

static int audioPipelineGoertzelRunning = 0;
void audioPipelineLink(int modus, char *fileName)
{
    ESP_LOGI(TAG, "[3.1] Link it together");
    const char *link_tag_http[3] = {"http", "mp3", "i2sw"};
    const char *link_tag_file[3] = {"file", "mp3", "i2sw"};
    const char *link_tag_sound[3] = {"i2sr", "rsp_filter", "raw"};

    switch (modus)
    {
    case 0:
        ESP_LOGI(TAG, "[3.1.1] Link SD");
        audioPipelineGoertzelRunning = 0;
        audioPipelineStopAndWait();
        audio_pipeline_link(pipeline, &link_tag_file[0], 3);
        char *filePath = malloc(strlen(fileName) + 14 + 4 + 1); // 14 => file://sdcard/ + 4 => .mp3 + 1 => end
        strcpy(filePath, "file://sdcard/");
        strcat(filePath, fileName);
        strcat(filePath, ".mp3");
        ESP_LOGI(TAG, "[3.2] Set up uri: ");
        audio_element_set_uri(fatfs_stream_reader, filePath);
        free(filePath);
        break;
    case 1:
        ESP_LOGI(TAG, "[3.1.1] Link Radio");
        audioPipelineGoertzelRunning = 0;
        audioPipelineStopAndWait();
        audio_pipeline_link(pipeline, &link_tag_http[0], 3);
        ESP_LOGI(TAG, "[3.2] Set up  uri (http as http_stream, mp3 as mp3 decoder, and default output is i2s)");
        audio_element_set_uri(http_stream_reader, fileName);
        break;
    case 2:
        ESP_LOGI(TAG, "[3.1.1] Link Goertzel");
        audioPipelineGoertzelRunning = 0;
        audioPipelineStopAndWait();
        audio_pipeline_link(pipeline, &link_tag_sound[0], 3);

        ESP_LOGI(TAG, "Setup Goertzel detection filters");
        for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
        {
            filters_cfg[f].sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
            filters_cfg[f].target_freq = GOERTZEL_DETECT_FREQS[f];
            filters_cfg[f].buffer_length = GOERTZEL_BUFFER_LENGTH;
            esp_err_t error = goertzel_filter_setup(&filters_data[f], &filters_cfg[f]);
            ESP_ERROR_CHECK(error);
        }
        audioPipelineGoertzelRunning = 1;
        xTaskCreate(audioAnalyseLoop, "audioAnalyseLoop", 4048, NULL, 1, NULL);
        break;
    }

    // Example of using an audio event -- START
    ESP_LOGI(TAG, "[ 4 ] Set up event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(pipeline, evt);

    // ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    // audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);
}

void audioAnalyseLoop(void *parameter)
{
    ESP_LOGI(TAG, "Create raw sample buffer");
    int16_t *raw_buffer = (int16_t *)malloc((GOERTZEL_BUFFER_LENGTH * sizeof(int16_t)));
    if (raw_buffer == NULL)
    {
        ESP_LOGE(TAG, "Memory allocation for raw sample buffer failed");
        vTaskDelete(NULL);
        return;
    }

    while (audioPipelineGoertzelRunning)
    {
        raw_stream_read(raw_reader, (char *)raw_buffer, GOERTZEL_BUFFER_LENGTH * sizeof(int16_t));
        for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
        {
            float magnitude;
            esp_err_t error = goertzel_filter_process(&filters_data[f], raw_buffer, GOERTZEL_BUFFER_LENGTH);
            ESP_ERROR_CHECK(error);

            if (goertzel_filter_new_magnitude(&filters_data[f], &magnitude))
            {
                detect_freq(filters_cfg[f].target_freq, magnitude);
            }
        }
        if (foundFreq)
        {
            static int isLCDon = 0;
            if (isLCDon)
            {
                lcdBacklightOff();
                isLCDon = 0;
            }
            else
            {
                lcdBacklightOn();
                isLCDon = 1;
            }
            vTaskDelay(2000 / portTICK_RATE_MS);
            foundFreq = 0;
        }
    }
    free(raw_buffer);
    vTaskDelete(NULL);
}

void audioPipelineLoop(void)
{
    audio_event_iface_msg_t msg;
    esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);

    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
        return;
    }

    if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)mp3_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
    {
        audio_element_info_t music_info = {0};
        audio_element_getinfo(mp3_decoder, &music_info);

        ESP_LOGI(TAG, "[ * ] Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                 music_info.sample_rates, music_info.bits, music_info.channels);

        audio_element_setinfo(i2s_stream_writer, &music_info);
        i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
        return;
    }

    /* Stop when the last pipeline element (i2s_stream_writer in this case) receives stop event */
    if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)i2s_stream_writer && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED)))
    {
        ESP_LOGW(TAG, "[ * ] Stop event received");
        SDIsRunning = 0;
    }
}

static esp_err_t input_key_service_cb(periph_service_handle_t handle, periph_service_event_t *evt, void *ctx)
{
    audio_board_handle_t local_board_handle = (audio_board_handle_t) ctx;
    if (evt->type == INPUT_KEY_SERVICE_ACTION_CLICK_RELEASE) 
    {
        ESP_LOGI(TAG, "Input key id is %d", (int)evt->data);
        if ((int)evt->data == INPUT_KEY_USER_ID_VOLDOWN)
        {
            ESP_LOGI(TAG, "Touch event volume down");
            volume -= 5;
            if (volume < 0)
            {
                volume = 0;
            }
        }
        else if ((int)evt->data == INPUT_KEY_USER_ID_VOLUP)
        {
            ESP_LOGI(TAG, "Touch event volume up");
            volume += 5;
            if (volume > 100)
            {
                volume = 100;
            }
        }
        audio_hal_set_volume(local_board_handle->audio_hal, volume);
        audio_hal_set_mute(local_board_handle->audio_hal, volume < 1);
        ESP_LOGI(TAG, "Volume is set to %d", volume);
    }
    return ESP_OK;
}

void audioPipelinePause(void)
{
    audio_element_pause(i2s_stream_writer);
}

void audioPipelineTerminate(void)
{
    ESP_LOGI(TAG, "[ 6 ] Stop audio_pipeline");
    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);

    audio_pipeline_remove_listener(pipeline);
}

void audioPipelineDestroy()
{
    /* Terminate the pipeline before removing the listener */
    audio_pipeline_unregister(pipeline, fatfs_stream_reader);
    audio_pipeline_unregister(pipeline, http_stream_reader);
    audio_pipeline_unregister(pipeline, i2s_stream_writer);
    audio_pipeline_unregister(pipeline, i2s_stream_reader);
    audio_pipeline_unregister(pipeline, mp3_decoder);

    /* Stop all peripherals before removing the listener */
    esp_periph_set_stop_all(set);
    audio_event_iface_remove_listener(esp_periph_set_get_event_iface(set), evt);

    /* Make sure audio_pipeline_remove_listener & audio_event_iface_remove_listener are called before destroying event_iface */
    audio_event_iface_destroy(evt);
    
    /* Release all resources */
    audio_pipeline_deinit(pipeline);
    audio_element_deinit(http_stream_reader);
    audio_element_deinit(i2s_stream_writer);
    audio_element_deinit(i2s_stream_reader);
    audio_element_deinit(mp3_decoder);
    esp_periph_set_destroy(set);
    periph_service_destroy(input_ser);
}

void audioPipelinePlaySD(char *fileName)
{
    audioPipelineLink(0, fileName);
    SDIsRunning = 1;
    while (SDIsRunning)
    {
        audioPipelineLoop();
        vTaskDelay(20);
    }
    audioPipelineTerminate();
}

static int radioLoopTaskRunning;
static void vRadioLoopTask(void *pvParameters)
{
    audioPipelineLink(1, channelMenuGetChannel());
    while (radioLoopTaskRunning)
    {
        audioPipelineLoop();
        vTaskDelay(20);
    }
    audioPipelineTerminate();
    vTaskDelete(NULL);
}

void audioPipelinePlayRadio(void)
{
    radioLoopTaskRunning = 1;
    xTaskCreate(vRadioLoopTask, "radioloop", 5000, NULL, 1, NULL);
}

void audioPipelineStopRadio(void)
{
    radioLoopTaskRunning = 0;
    audio_element_pause(i2s_stream_writer);
    vTaskDelay(20);
    audioPipelineLink(2, NULL);
}

void audioPipelineResetRadioChannel(void)
{
    if (radioLoopTaskRunning)
    {
        audioPipelineStopRadio();
        audioPipelinePlayRadio();
    }
}

int audioPipelineGetVolume()
{
    return volume;
}
