#pragma once

void speakCurrentTime(void);

void speakStudentAlarm(void);

void speakStudentBreakOver(void);

void speakTimeToSleep(void);
