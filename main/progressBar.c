#include "progressBar.h"
#include <stdlib.h>
#include "lcd.h"
#include "displays/alarmGetters.h"
#include "displays/menuGetters.h"

#define ROW_NUMBER 3 // The row number to which the progress bar is drawn. (0 indexed)
#define MAX_ROWS 8 // Number of rows per character.
#define MAX_COLUMNS 5 // Number of columns per character.
#define MAX_CHARACTERS 20 // Number of characters per row.

#define ONE_COLUMN I2C_LCD1602_CHARACTER_CUSTOM_0
#define TWO_COLUMN I2C_LCD1602_CHARACTER_CUSTOM_1
#define THREE_COLUMN I2C_LCD1602_CHARACTER_CUSTOM_2
#define FOUR_COLUMN I2C_LCD1602_CHARACTER_CUSTOM_3
#define FIVE_COLUMN I2C_LCD1602_CHARACTER_CUSTOM_4

short column_number = 0;

/**
 * @brief Creates a custom character that is one or more vertical lines.
 * The vertical lines will all be left alligned.
 * 
 * @param columns_to_fill the amount of vertical lines this character will have. 0 <= columns_to_fill <= MAX_COLUMNS
 * @return uint8_t* the array containing the bytes with the information to draw the character.
 */
uint8_t* create_custom_character(short columns_to_fill)
{
    columns_to_fill = MAX_COLUMNS - columns_to_fill;

    // Guard clause, invalid amount of columns.
    if (columns_to_fill < 0 || columns_to_fill > MAX_COLUMNS)
    {
        return malloc(MAX_ROWS); // Retrun empty array;
    }

    uint8_t* temp_array = malloc(MAX_ROWS);

    for (int i = 0; i < MAX_ROWS; i++)
    {
        // Initialize all values to 0.
        temp_array[i] = 0;
    }

    for (int i = 0; i < MAX_ROWS; i++)
    {
        for (int j = 0; j < columns_to_fill; j++)
        {
            temp_array[i] |= (1 << j);
        }

        temp_array[i] ^= 0b11111; // Invert the output to make it allign left.
    }

    return temp_array;
}

/**
 * @brief Initializes the custom characters used for the
 * progress bar.
 * 
 * The characters are stored in 
 * @code i2c-lcd1602.h I2C_LCD1602_CHARACTER_CUSTOM_n
 * 
 * n being the number for the character.
 * 
 * 0 <= n <= 4
 * 
 * Higher n equals to a more filled up character on the lcd.
 * 
 */
void init_custom_characters()
{
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_0, create_custom_character(1));
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_1, create_custom_character(2));
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_2, create_custom_character(3));
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_3, create_custom_character(4));
    i2c_lcd1602_define_char(lcd_info, I2C_LCD1602_INDEX_CUSTOM_4, create_custom_character(5));
}

/**
 * @brief This method calculates the number of steps that are "completed".
 * This can be used to see the percentage of the progress bar that should
 * be filled.
 * 
 * @return short -1 if the alarm is longer than one hour away from the
 * current time.
 * 
 * @return short 0 - 100 % indicating the completion of the final hour
 * towards the alarm.
 */
short calculate_number_of_steps()
{
    int alarm_total_minutes = alarmGetHours() * 60 + alarmGetMinutes();
    int menu_total_minutes = mainMenuGetHours() * 60 + mainMenuGetMinutes();

    // Guard clause, alarm is more than one hour in the future so return -1.   
    if (alarm_total_minutes - menu_total_minutes > 60 || alarm_total_minutes - menu_total_minutes < 0)
    {
        return -1;
    }

    return 100 - (alarm_total_minutes - menu_total_minutes) / 60.0 * 100.0;
}

/**
 * @brief This method repeatedly writes the given character to the screen on
 * line: @code ROW_NUMBER
 * 
 * @param character the character that will be written to the lcd.
 * @param repeat_amount the amount of times the given character wil be written.
 */
void write_to_lcd(uint8_t character, short repeat_amount)
{
    for (short i = 0; i < repeat_amount; i++)
    {
        i2c_lcd1602_move_cursor(lcd_info, column_number, ROW_NUMBER);
        i2c_lcd1602_write_char(lcd_info, character);

        // Keep increasing the column number, but prevent it from going off the screen.
        if (column_number < MAX_CHARACTERS)
        {
            column_number++;
        }
        else
        {
            column_number = 0;
        }
    }

    if(repeat_amount == 1) 
    {
        column_number = 0; // Reset the cursor to the start after the final print.
    }
}

/* Explenation in header file. */
void draw_progress_bar()
{
    column_number = 0; // Reset the cursur to the start when the screen is entirely redrawn.

    short num_of_steps = calculate_number_of_steps();

    // Alarm is further out than one hour, so don't draw progress bar.
    if (num_of_steps == -1)
    {
        return;
    }

    init_custom_characters();

    short num_of_whole_steps = num_of_steps / MAX_COLUMNS; // Gets floored
    write_to_lcd(FIVE_COLUMN, num_of_whole_steps);

    short remaining_steps = num_of_steps % MAX_COLUMNS;

    switch (remaining_steps)
    {
        case 0:
        {
            break; // All the steps are whole steps and are already drawn above.
        }
        case 1:
        {
            write_to_lcd(ONE_COLUMN, 1);
            break;
        }
        case 2:
        {
            write_to_lcd(TWO_COLUMN, 1);
            break;
        }
        case 3:
        {
            write_to_lcd(THREE_COLUMN, 1);
            break;
        }
        case 4:
        {
            write_to_lcd(FOUR_COLUMN, 1);
            break;
        }
    }
}
