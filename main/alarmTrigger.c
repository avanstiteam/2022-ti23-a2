#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "alarmTrigger.h"
#include "displays/alarmGetters.h"
#include "displays/studentAlarmGetters.h"
#include "displays/timeToSleepGetters.h"
#include "displays/menuGetters.h"
#include "displays/alarmModus.h"
#include "displays/alarmStudent.h"
#include "audioPipeline.h"
#include "audioFileSelector.h"

static int alarmIsRunning;

void alarmCheckTime()
{
    int alarmHours = alarmGetHours();
    int alarmMinutes = alarmGetMinutes();
    int ttsHours = ttsGetHours();
    int ttsMinutes = ttsGetMinutes();
    int currentHours = mainMenuGetHours();
    int currentMinutes = mainMenuGetMinutes();
    int studentPauseActive = pauseStudentIsActive();
    int studentAlarmActive = alarmStudentIsActive();

    if (alarmHours == currentHours && alarmMinutes == currentMinutes)
    {
        printf("ALARM!\n");
        alarmIsRunning = 1;

        switch (alarmGetModus())
        {
        case RADIOMODE:
            audioPipelinePlayRadio();
            break;
        case SDMODE:
            speakCurrentTime();
            break;
        }
    }
    else
    {
        if (alarmIsRunning)
        {
            alarmIsRunning = 0;
            if (alarmGetModus() == RADIOMODE)
            {
                audioPipelineStopRadio();
            }
        }
    }

    // Checks if student alarm is active, if it is it will check if it has to go off.
    if (studentAlarmActive)
    {
        studentAlarmCheck();
    }

    // Checks if the Student Pause is active, if it is it will check if it has to go off.
    if (studentPauseActive)
    {
        studentPauseCheck();
    }

    if (ttsHours == currentHours && ttsMinutes == currentMinutes)
    {
        printf("Time to sleep!\n");
        if (ttsGetActive())
        {
            speakTimeToSleep();
        }
    }
}
