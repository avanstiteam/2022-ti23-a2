#include "a2_button.h"
void vButtonTask(void *pvParameters);

void button_update(Button_t* buttons, int size)
{
    for (int i = 0; i < size; i++)
    {
        Button_t* button = &buttons[i];
        uint8_t readed = a2_read(button->gpio);

        if (readed & button->mask)
        {
            button->pressed = 1;
        }
        else
        {
            button->pressed = 0;
            button->handled = 0;
        }

        if(button->pressed && !button->handled)
        {
            (*button->handler)();
            button->handled = 1;
        }
    }
}

void a2_button_init(Button_t* buttons, int size)
{
    ButtonTaskArguments *buttonArgumentPtr = malloc(sizeof(ButtonTaskArguments));
    buttonArgumentPtr->buttonArray = buttons;
    buttonArgumentPtr->arraySize = size;

    xTaskCreate(&vButtonTask, "button update", 5000, buttonArgumentPtr, 1, NULL);
}

void vButtonTask(void *pvParameters)
{
    ButtonTaskArguments *buttonArgumentPtr = (ButtonTaskArguments *)pvParameters;

    for(;;)
    {
        button_update(buttonArgumentPtr->buttonArray, buttonArgumentPtr->arraySize);
        vTaskDelay(15 / portTICK_RATE_MS);
    }
    vTaskDelete(NULL);
}