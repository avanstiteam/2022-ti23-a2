#pragma once
#include "gpio.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

typedef struct {
    int pressed;
    int handled;
    void (*handler)(void);

    GioPortT gpio;
    int mask;
} Button_t;

typedef struct{
    Button_t* buttonArray;
    int arraySize;
} ButtonTaskArguments;

void a2_button_init(Button_t* buttons, int size);