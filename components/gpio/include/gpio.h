#pragma once

// #include <mcp23017.h>
// #include <i2c_bus.h>
#include "../../esp32-smbus/include/smbus.h"
// #include <driver/i2c.h>
#include <esp_log.h>

#define I2C_MASTER_SCL_IO 23        /*!< gpio number for I2C master clock IO21*/
#define I2C_MASTER_SDA_IO 18        /*!< gpio number for I2C master data  IO15*/
#define I2C_MASTER_NUM I2C_NUM_0    /*!< I2C port number for master dev */
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master do not need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master do not need buffer */
#define I2C_MASTER_FREQ_HZ 100000   /*!< I2C master clock frequency */

typedef enum {
    PINA = 0x00,
    PINB
} GioPortT;

#define PIN_A_REGISTER 18
#define PIN_A_DIR_REGISTER 0
#define PIN_B_REGISTER 19
#define PIN_B_DIR_REGISTER 1

#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled

#define LOG_TAG "GPIO"

void a2_gpio_init();
void a2_gpio_deinit();

esp_err_t a2_gpio_direction_set(uint8_t value, GioPortT gpio);

uint8_t a2_read(GioPortT gpio);
uint8_t a2_read_bit(GioPortT gpio, int bit);
esp_err_t a2_write(GioPortT gpio, uint8_t value);
esp_err_t a2_write_bit(GioPortT gpio, int bit, int value);