#include "gpio.h"

// static i2c_bus_handle_t i2c_bus = NULL;
// static mcp23017_handle_t device = NULL;

static smbus_info_t * smbus_info = NULL;
static i2c_port_t i2c_num;
static SemaphoreHandle_t xSemaphore;

void a2_gpio_init()
{
    i2c_config_t conf = {
        .clk_flags = 0,
        .mode = I2C_MODE_MASTER,
        .sda_io_num = I2C_MASTER_SDA_IO,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_io_num = I2C_MASTER_SCL_IO,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = I2C_MASTER_FREQ_HZ,
    };
    
    i2c_num = I2C_MASTER_NUM;
    i2c_param_config(i2c_num, &conf);
    i2c_driver_install(i2c_num, (&conf)->mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);

    uint8_t address = 0x20;

    // Set up the SMBus
    smbus_info = smbus_malloc();
    smbus_init(smbus_info, i2c_num, address);
    smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);

    xSemaphore = xSemaphoreCreateMutex();
    if(xSemaphore == NULL)
    {
        ESP_LOGE(LOG_TAG, "Can't create Semaphore");
    }
}

void a2_gpio_deinit()
{
    smbus_free(&smbus_info);
    smbus_info = NULL;
    i2c_driver_delete(i2c_num);
}

esp_err_t a2_gpio_direction_set(uint8_t value, GioPortT gpio)
{
    if(xSemaphoreTake( xSemaphore, portMAX_DELAY) != pdTRUE)
    {
        ESP_LOGE(LOG_TAG, "Can't take semaphore");
        return ESP_FAIL;
    }

    esp_err_t error = smbus_write_byte(smbus_info, (gpio == PINA) ?
                               PIN_A_DIR_REGISTER : PIN_B_DIR_REGISTER, value);

    xSemaphoreGive( xSemaphore );

    return error;
}

uint8_t a2_read(GioPortT gpio)
{
    if(xSemaphoreTake( xSemaphore, portMAX_DELAY) != pdTRUE)
    {
        ESP_LOGE(LOG_TAG, "Can't take semaphore");
        return ESP_FAIL;
    }

    uint8_t data = 0;
    smbus_read_byte(smbus_info, (gpio == PINA) ? PIN_A_REGISTER : PIN_B_REGISTER, &data);
    
    xSemaphoreGive( xSemaphore );
    
    return data;
}

uint8_t a2_read_bit(GioPortT gpio, int bit)
{
    return a2_read(gpio) & (1 << bit);
}

esp_err_t a2_write(GioPortT gpio, uint8_t value)
{
    if(xSemaphoreTake( xSemaphore, portMAX_DELAY) != pdTRUE)
    {
        ESP_LOGE(LOG_TAG, "Can't take semaphore");
        return ESP_FAIL;
    }

    esp_err_t error = smbus_write_byte(smbus_info, (gpio == PINA) ? PIN_A_REGISTER : PIN_B_REGISTER, value);
    
    xSemaphoreGive( xSemaphore );

    return error;
}

esp_err_t a2_write_bit(GioPortT gpio, int bit, int value)
{
    uint8_t old_value = a2_read_bit(gpio, bit);
    uint8_t new_value = (old_value | ((value & 1) << bit));
    return a2_write(gpio, new_value);
}