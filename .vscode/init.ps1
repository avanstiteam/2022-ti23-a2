
$wsName = $args[0];
$target = $args[1];

(Get-Content CMakeLists.txt) -replace '%proj_name%', $wsName | Out-File -encoding utf8 CMakeLists.txt
(Get-Content Makefile) -replace '%proj_name%', $wsName | Out-File -encoding utf8 Makefile

idf.py set-target $target